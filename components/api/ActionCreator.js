import axios from 'axios';
import { fetchData, fetchSuccess, fetchError } from "./Action";

const actionCreator = url => dispatch => {
  return new Promise(function (resolve, reject) {
    axios
      .get(url)
      .then(response => {
        dispatch(fetchSuccess(response.data));
        resolve(fetchSuccess(response.data));
      })
      .catch(error => {
        dispatch(fetchError(error));
        reject(fetchError(error));
      })
  })
}

export default actionCreator