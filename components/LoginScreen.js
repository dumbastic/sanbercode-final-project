import React, { Component } from 'react'
import { View, Image, Text, StyleSheet, TextInput, TouchableOpacity } from 'react-native'

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userName: '',
            password: '',
            isError: false,
        }
        this.loginHandler = this.loginHandler.bind(this)
    }
    
    loginHandler() {
        if (this.state.userName === 'admin' && this.state.password === '12345') {
            this.props.navigation.reset({
                index: 0,
                routes: [{ name: 'Football Highlights' }],
            })
        }
        else {
            this.setState({ isError:true })
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../assets/logo.png')} />
                <Text style={[styles.title, styles.darkBlue]}>Please Sign In</Text>
                <View style={styles.inputView}>
                    <Text style={[styles.textSmall, styles.darkBlue]}>Username</Text>
                    <TextInput style={styles.inputText} placeholder="admin"
                        onChangeText={userName => this.setState({userName})} />
                </View>
                <View style={styles.inputView}>
                    <Text style={[styles.textSmall, styles.darkBlue]}>Password</Text>
                    <TextInput style={styles.inputText} secureTextEntry={true} placeholder="12345"
                        onChangeText={password => this.setState({password})} />
                </View>
                <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>
                    Username/Password Salah
                </Text>
                <TouchableOpacity style={styles.button} onPress={() => this.loginHandler()}>
                    <Text style={styles.buttonText}>SIGN IN</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    title: {
        fontSize: 24,
        margin: 50
    },
    inputView: {
        width: "80%",
        height: 48,
        marginBottom: 30,
        justifyContent: "center"
    },
    inputText: {
        height: 48,
        padding: 10,
        fontSize: 20,
        backgroundColor: "white",
        borderColor: "#003366",
        borderWidth: 1
    },
    textSmall: {
        fontSize: 16,
        alignSelf: "flex-start"
    },
    textBig: {
        fontSize: 24
    },
    errorText: {
      color: 'red',
      textAlign: 'center',
      marginBottom: 10,
    },
    hiddenErrorText: {
      color: 'transparent',
      textAlign: 'center',
      marginBottom: 10,
    },
    button:{
        backgroundColor: "red",
        width: "80%",
        borderRadius: 8,
        height: 50,
        alignItems: "center",
        justifyContent: "center"
    },
    buttonText: {
        fontSize: 20,
        color: "white"
    }
  });