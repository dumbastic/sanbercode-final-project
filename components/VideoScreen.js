import React from 'react'
import { ScrollView, View, Text, StyleSheet, Dimensions } from 'react-native'
import { WebView } from 'react-native-webview'

const DEVICE = Dimensions.get('window')

export default function Video({ navigation, route }) {
    navigation.setOptions({ 
        title: route.params.data.side1.name +  ' vs ' + route.params.data.side2.name 
    })

    var videos = []
    for (var i = 0; i < route.params.data.videos.length; i++) {
        videos.push({
            "title": route.params.data.videos[i].title,
            "uri": route.params.data.videos[i].embed.match(/\bhttps?:\/\/\S[^']+/gi)
        })
    }
    
    return(
        <ScrollView>
            { videos.map((video) => 
                (
                    <View key={video.title} style={styles.container}>
                        <Text style={styles.text}>{video.title}</Text>
                        <WebView style={{ height: DEVICE.height*0.3 }}
                            source={{ uri: video.uri.toString() }}
                            allowsFullscreenVideo={true} />
                    </View>
                )
            )}
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "black"
    },
    text: {
        fontSize: 16,
        fontWeight: "bold",
        color: "white",
        alignSelf: "center",
        marginTop: 10
    }
})