import React, { Component } from 'react'
import { View, ScrollView, Image, Text, StyleSheet, TouchableOpacity, Dimensions } from 'react-native'

import { connect } from "react-redux"
import apiCall from "./api/ActionCreator"

import { FontAwesome } from '@expo/vector-icons';

const DEVICE = Dimensions.get('window')

class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            competition: []
        }
    }

    componentDidMount() {
        this.props
            .apiCall("https://www.scorebat.com/video-api/v1/")
            .then(() => {
                const data = this.props.data
                this.setState({ data })
                
                var lookup = {};
                var items = data;
                var competition = [];

                for (var item, i = 0; item = items[i++];) {
                    var id = item.competition.id;
                    var name = item.competition.name;

                    if (!(id in lookup)) {
                        lookup[id] = 1;
                        competition.push({ id, name });
                    }
                }
                
                function compare(a, b) {
                    let comparison = 0;
                    if (a.id > b.id) {
                      comparison = 1;
                    } else if (a.id < b.id) {
                      comparison = -1;
                    }
                    return comparison;
                }
                
                competition.sort(compare)
                this.setState({ competition })
            })
            .catch(error => {
                console.log(error);
            })
    }

    render() {
        return(
            <View style={styles.container}>
                <Image source={require('../assets/logo.png')} style={{alignSelf: "center", margin: 10}} />
                <ScrollView>
                    { this.state.competition.map((competition, index) =>
                        (
                            <View key={competition.id}
                                style={[styles.league, {backgroundColor: index % 2 == 0  ? "#FFFFFF" : "#F2F2F2"}]}>
                                <TouchableOpacity style={{flexDirection: "row"}} 
                                    onPress={() => this.props.navigation.navigate('Thumbnail', 
                                    {data: this.state.data, competition: competition})}>
                                    <FontAwesome name="youtube-play" size={24} color="red" style={{marginRight:10}} />
                                    <Text style={styles.text}>{competition.name}</Text>
                                </TouchableOpacity>
                            </View>
                        )
                    )}
                </ScrollView>
            </View>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    apiCall: url => dispatch(apiCall(url))
});
  
const mapStateToProps = state => ({
    data: state.apiReducer.data,
    error: state.apiReducer.error,
});
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    league: {
        width: "100%",
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 15,
        paddingRight: 15
    },
    text: {
        fontSize: 18,
        fontWeight: "bold"
    },
    image: {
        width: "90%",
        height: DEVICE.height * 0.25
    }
})