import React from 'react'
import { View, Text, StyleSheet, Linking, Image } from 'react-native'

export default function About({ navigation }) {
    return(
        <View style={styles.container}>
            <Image source={require('../assets/logo.png')} />
            <Text style={styles.text}>
                This application was developed using{"\n"}React Native + Expo based on ScoreBat free API:{"\n"}
                <Text style={{ color: 'blue', textDecorationLine:"underline" }}
                    onPress={() => Linking.openURL('https://www.scorebat.com/video-api/')}>
                    https://www.scorebat.com/video-api/
                </Text>{"\n"}{"\n"}{"\n"}
                For more information, you can contact me:{"\n"}
                Dommy Asfiandy (
                <Text style={{ color: 'blue', textDecorationLine:"underline" }}
                    onPress={() => Linking.openURL('mailto:dumbastic@gmail.com')}>
                    dumbastic@gmail.com
                </Text>)
            </Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    text: {
        textAlign: "center",
        fontSize: 16,
        padding: 10,
        marginTop: 50
    }
})