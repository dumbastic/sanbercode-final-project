import React, { Component } from 'react'
import { View, FlatList, Image, Text, TouchableOpacity, Dimensions, StyleSheet } from 'react-native'

import Moment from 'moment'
import { FontAwesome } from '@expo/vector-icons'

const DEVICE = Dimensions.get('window')

export default class Thumbnail extends Component {
    render() {
        const data = this.props.route.params.data
        const competition = this.props.route.params.competition

        return(
            <View style={styles.container}>
                <View style={{flexDirection:"row"}}>
                    <FontAwesome name="youtube-play" size={24} color="red" style={{marginRight:10}} />
                    <Text style={styles.titleText}>{competition.name}</Text>
                </View>
                <FlatList numColumns={2} 
                    data={data.filter(x => x.competition.id === competition.id)}
                    renderItem={(data) => <ListItem data={data.item} navigation={this.props.navigation} />}
                    keyExtractor={(item) => item.title}
                />
            </View>
        )
    }
}

class ListItem extends Component {
    render() {
        const data = this.props.data
        
        return (
            <View style={styles.itemContainer}>
                <TouchableOpacity 
                    onPress={() => this.props.navigation.navigate('Video', { data: data })}>
                    <Image source={{ uri: data.thumbnail }} style={styles.itemImage} resizeMode='contain' />
                    <Text numberOfLines={1} ellipsizeMode='tail' style={styles.itemTitle}>
                        {data.title}
                    </Text>
                    <Text numberOfLines={1} ellipsizeMode='tail' style={styles.itemDate}>
                        {Moment(data.date).format('dddd, D MMMM yyyy')}
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        paddingTop: 10
    },
    titleText: {
        fontSize: 18,
        fontWeight: "bold",
        paddingBottom: 10
    },
    itemContainer: {
        width: DEVICE.width * 0.44,
        marginLeft: 11,
        marginRight: 11,
        marginBottom: 10
    },
    itemImage: {
        width: 200,
        height: 160,
        alignSelf: "center",
        borderRadius: 8
    },
    itemTitle: {
        fontSize: 14
    },
    itemDate: {
        fontSize: 12
    }
})