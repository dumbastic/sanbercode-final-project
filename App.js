import React from 'react'

import { NavigationContainer, DefaultTheme } from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

import { Provider } from "react-redux";
import Store from "./components/api/Store";

import LoginScreen from './components/LoginScreen'
import HomeScreen from './components/HomeScreen'
import ThumbnailScreen from './components/ThumbnailScreen'
import VideoScreen from './components/VideoScreen'
import AboutScreen from './components/AboutScreen'

const Home = createStackNavigator()
const HomeStack = () => (
    <Home.Navigator>
        <Home.Screen name="Home" component={HomeScreen} />
    </Home.Navigator>
)

const Tab = createBottomTabNavigator()
const TabScreen = () => (
    <Tab.Navigator>
        <Tab.Screen name="Home" component={HomeStack}
          options={{
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="home" color={color} size={size} />
            ),
          }} />
        <Tab.Screen name="About" component={AboutScreen}
          options={{
              tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="cellphone-information" color={color} size={size} />
            ),
          }} />
    </Tab.Navigator>
)

const RootStack = createStackNavigator()
const RootStackScreen = () => (
    <RootStack.Navigator>
        {/* <RootStack.Screen name="Sign In" component={LoginScreen} /> */}
        <RootStack.Screen name="Football Highlights - Competitions" component={TabScreen} />
        <RootStack.Screen name="Thumbnail" component={ThumbnailScreen} options={{ title: 'Football Highlights - Matches' }} />
        <RootStack.Screen name="Video" component={VideoScreen} />
    </RootStack.Navigator>
)

export default function App() {
  return (
    <Provider store={Store}>
      <NavigationContainer>
        <RootStackScreen />
      </NavigationContainer>
    </Provider>    
  );
}

const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: 'F2F2F2'
  },
};
